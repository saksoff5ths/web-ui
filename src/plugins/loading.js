import Vue from 'vue'

const loading = new Vue({
  data: {
    isShow: false,
  },
  methods: {
    show () {
      this.isShow = true
    },
    hide () {
      this.isShow = false
    }
  }
})

export default {
  install (Vue) {
    Vue.prototype.$loading = loading
  }
}
