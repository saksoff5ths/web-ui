// import store from '../store'
import router from '../router'
import axios from 'axios'
import VueAxios from 'vue-axios'

import {
    BASE_URL,
    API_SERVER,
    // AUTHORIZATION
} from '@/constants/constants'
// import {setAuth} from '../utils/auth'

axios.defaults.baseURL = API_SERVER + BASE_URL
axios.interceptors.request.use(config => {
    // if (store.getters.currentToken) {
    //     config.headers.common[AUTHORIZATION] = 'Bearer ' + store.getters.currentToken
    // }
    return config
}, error => {
    return Promise.reject(error)
})
axios.interceptors.response.use(response => {
    if (response.status === 401) {
        // store.dispatch('resetAuth')
        router.push('/login').catch(() => {
        })
    }
    return response
}, error => {
    if (error.response.status === 401) {
        // store.dispatch('resetAuth')
        router.push('/login').catch(() => {
        })
    }
    return Promise.reject(error)
})

const http = {
    get(url, params) {
        return this.request('get', url, params)
    },
    put(url, params) {
        return this.request('put', url, params)
    },
    post(url, params) {
        return this.request('post', url, params)
    },
    delete(url, params) {
        return this.request('delete', url, params)
    },
    request(type, url, params) {
        // const expiresIn = store.getters.expiresIn
        // const refreshToken = store.getters.refreshToken
        // if (!expiresIn || !refreshToken) {
        //     return axios[type](url, params)
        // }
        // const currentSec = Math.floor(Date.now() / 1000)
        // const expiresSec = Math.floor((Date.now() / 1000) + (+expiresIn))
        return axios[type](url, params)
    },
}
// export default http;

export default {
    install(Vue) {
        Vue.use(VueAxios, axios)
        Vue.prototype.$httpRequest = http
    }
}
