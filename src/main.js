import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from './plugins/axios'
import loading from './plugins/loading'

Vue.config.productionTip = false
Vue.use(axios)
Vue.use(loading)

new Vue({
    router,
    vuetify,
    store,
    render: h => h(App)
}).$mount('#app')
