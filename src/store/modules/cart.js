const state = {
    items: [],
}

const actions = {
    addToCart({commit}, product) {
        commit('addToCart', product)
    }
}

const mutations = {
    addToCart(state, product) {
        state.items.push(product);
    }
}

const getters = {
    getCart: (state) => {
        return state.items || [];
    }
}

export default {
    state,
    actions,
    mutations,
    getters
}