import axios from 'axios'

const state = {
    token: null
}
const actions = {
    setToken({commit}, product) {
        commit('setToken', product)
    },
    login ({ commit }, param) {
        return axios.post('http://localhost:8080/api/v1/login', param)
            .then(response => {
                if (response.status === 200 && response.data) {
                    const { data } = response
                    commit('setToken', data)
                }
                return response
            }).catch(error => {
                return error.response
            })
    },
}

const mutations = {
    setToken(state, data) {
        state.token = data.token
    }
}

const getters = {
    getToken: (state) => {
        return state.token;
    }
}

export default {
    state,
    actions,
    mutations,
    getters
}