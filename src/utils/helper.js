import store from '../store'

export const beforeEnter = (to, from, next) => {
  if (!store.getters.getToken) {
    next('/login')
  } else {
    next()
  }
}
