import Vue from "vue";
import VueRouter from 'vue-router'
import {beforeEnter} from "@/utils/helper";

import HomePage from "@/views/HomePage";
import CategoryPage from "@/views/CategoryPage";
import CheckoutPage from "@/views/CheckoutPage";
import LoginPage from "@/views/LoginPage";

Vue.use(VueRouter);
const routes = [
    {
        path: "/",
        component: HomePage,
        name: "HomePage"
    },
    {
        path: "/category/:id?",
        component: CategoryPage,
        name: "CategoryDetail"
    },
    {
        path: "/checkout",
        component: CheckoutPage,
        name: "CheckoutPage",
        beforeEnter: beforeEnter,
    },
    {
        path: "/login",
        component: LoginPage,
        name: "LoginPage"
    }
];

const router = new VueRouter({
    mode: 'hash',
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    routes
})

export default router